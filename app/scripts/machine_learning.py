from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import nltk
import numpy as np

from app.repository.repository import get_antonyms_from_database
from app.scripts.models.models import WordList

def calculate_complexity_score_from_text(text_data, session):
    # Extract words from the database based on antonym status
    antonyms = get_antonyms_from_database(session)

    # Get all words from database for counting
    reductionistic_words = [word.word for word in
                            session.query(WordList).filter(WordList.type == 'reductionistic').all()]

    complex_words = [word.word for word in session.query(WordList).filter(WordList.type == 'complex').all()]

    # Calculate reductionistic and complex count scores
    complex_count = 0
    reductionistic_count = 0
    splitted_data = text_data.split()

    # Loop through all split text
    for i in range(1, len(splitted_data)):
        current_word = splitted_data[i]
        previous_word = splitted_data[i - 1] if i > 0 else None

        # check word if it exists on reductionistic words
        if current_word in reductionistic_words:
            result = session.query(WordList.gewicht).filter(WordList.word == current_word).first()

            if result is not None:
                gewicht = result[0]  # Take the weight out of the result

                # check previous word if it exists on antonyms
                if previous_word in antonyms:
                    complex_count += gewicht
                else:
                    reductionistic_count += gewicht

        # check word if it exists on complex words
        elif current_word in complex_words:
            result = session.query(WordList.gewicht).filter(WordList.word == current_word).first()

            if result is not None:
                gewicht = result[0]  # Take the weight out of the result

                # check previous word if it exists on antonyms
                if previous_word in antonyms:
                    reductionistic_count += gewicht
                else:
                    complex_count += gewicht

    # Calculate complexity score: difference between counts of complex and reductionistic words
    complexity_score = complex_count - reductionistic_count

    return int(complexity_score)


# The functions down below belong to René but aren't used anywhere in the project
def tfidf_and_bow(text_data, all_text_data):
    # Convert single string to list of strings
    if isinstance(text_data, str):
        text_data = [text_data]

    if isinstance(all_text_data, str):
        all_text_data = [all_text_data]

    # TF-IDF vectorization
    stopwords = nltk.corpus.stopwords.words('dutch')
    vectorizer_tfidf = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df=0.0, max_df=1.0,
                                       stop_words=stopwords)
    tfidf_matrix = vectorizer_tfidf.fit_transform(text_data)

    # Calculate cosine similarity for TF-IDF matrix
    cos_dist = 1 - cosine_similarity(tfidf_matrix)

    # Manual computation of the word occurrences (using TFIDF)
    count_vectorizer = TfidfVectorizer(analyzer='word', use_idf=False, stop_words=stopwords)
    count_matrix = count_vectorizer.fit_transform(all_text_data)

    # Compute cosine similarity manually (no TFIDF)
    vectors = count_matrix.toarray()
    num_vectors = vectors.shape[0]
    cos_sim = np.zeros((num_vectors, num_vectors))

    for i in range(num_vectors):
        for j in range(num_vectors):
            dot_product = np.dot(vectors[i], vectors[j])
            norm_i = np.linalg.norm(vectors[i])
            norm_j = np.linalg.norm(vectors[j])

            cos_sim[i][j] = dot_product / (norm_i * norm_j)

    cos_dist_manual = 1 - cos_sim

    # Calculate complexity score of word occurrences
    complexity_score_word_occurrences = calculate_complexity_score_word_occurrences(text_data)[0]
    word_counts_dict = calculate_complexity_score_word_occurrences(text_data)[1]

    # Calculate now for all pages in magazine
    complexity_score_word_occurrences_magazine = calculate_complexity_score_word_occurrences(all_text_data)[0]
    word_counts_dict_magazine = calculate_complexity_score_word_occurrences(all_text_data)[1]

    return (word_counts_dict, word_counts_dict_magazine, cos_dist, cos_dist_manual, complexity_score_word_occurrences,
            complexity_score_word_occurrences_magazine)


def calculate_complexity_score_word_occurrences(text_data):
    reductionistic_words = ['reductie', 'eenvoudig', 'enkelvoudig', 'weglaten', 'weggelaten', 'buiten beschouwing',
                            'exclusie', 'unifactorieel', 'causaliteit', 'oorzaak', 'gevolg', 'overzichtelijk',
                            'beperkt', 'beperking', 'gelimiteerd', 'afgebakend', 'lineair', 'reactie']
    complex_words = ['complex', 'gecompliceerd', 'interactie', 'multifactorieel', 'ingewikkeld', 'biopsychosociaal',
                     'situationeel', 'contextueel', 'gestalt', 'non-lineair', 'meerdimensionaal', 'onoverzichtelijk',
                     'moeilijk']

    # Word Counts using CountVectorizer
    vectorizer_counts = CountVectorizer(vocabulary=reductionistic_words + complex_words)
    word_counts = vectorizer_counts.fit_transform(text_data if isinstance(text_data, list) else [text_data])

    word_occurrences = np.asarray(word_counts.sum(axis=0)).reshape(-1)
    word_counts_dict = dict(zip(reductionistic_words + complex_words, word_occurrences))

    # Calculate complexity score: difference between counts of complex and reductionistic words
    complexity_score_word_occurrences = word_occurrences[len(reductionistic_words):].sum() - word_occurrences[:len(
        reductionistic_words)].sum()

    return complexity_score_word_occurrences, word_counts_dict
