import os
from names_dataset import NameDataset
from datetime import datetime
from PyPDF2 import PdfReader
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from app.repository.repository import (post_magazine, post_page, get_magazines, get_magazine_by_id,
                                       get_pages_by_magazine_id, get_antonyms_from_database)
from app.scripts.models.models import Magazine, Page, WordList
from .machine_learning import calculate_complexity_score_from_text
from cryptography.hazmat.primitives import hashes


def extract_and_upload_pdf(pdf, date_str, socketio, session):
    # Check if a magazine with the same title already exists
    existing_magazines = get_magazines(session)
    existing_hashes = [magazine.hashvalue for magazine in existing_magazines]
    existing_titles = [magazine.title for magazine in existing_magazines]
    publication_date = datetime.strptime(date_str, '%Y-%m-%d').date()
    hashvalue = generate_hash(pdf)

    if hashvalue in existing_hashes or pdf.filename in existing_titles:
        return 'De PDF is al een keer geüpload! Probeer een andere'

    emit_progress_update(socketio, 'PDF verwerken...', 0)
    reader = PdfReader(pdf)
    magazine = Magazine(publication_date=publication_date, upload_date=datetime.now(),
                        title=os.path.basename(pdf.filename), hashvalue=hashvalue, complexity_score=0.0,
                        reductionistic_score=0.0)
    persisted_magazine = post_magazine(magazine, session)

    first_page = True
    page_number = 0
    num_pages = len(reader.pages)
    for page in reader.pages:
        emit_progress_update(socketio, f'Pagina {page_number} van {num_pages} aan het verwerken...',
                             int((page_number / num_pages) * 100) - 5)
        page_number += 1
        if not first_page:
            parts = []

            def visitor_body(text, cm, tm, fontDict, fontSize):
                y = tm[5]
                if 50 < y < 720:
                    parts.append(text)

            page.extract_text(visitor_text=visitor_body)
            text_body = "".join(parts)

            num_words = len(text_body.split())

            if num_words >= 100:
                # Only save the page if the number of words is at least 100
                complexity_score = calculate_complexity_score_from_text(text_body, session)
                page = Page(text=text_body, page_number=page_number, complexity_score=complexity_score,
                            magazine_id=persisted_magazine.id)
                post_page(page, session)

        first_page = False

    emit_progress_update(socketio, 'Complexiteitsscores voor vaktijdschrift berekenen...', 95)
    calculate_scores_for_magazine(persisted_magazine.id, session)
    emit_progress_update(socketio, 'Verwerking voltooid!', 100)
    return 'De PDF is correct geüpload! Bekijk de resultaten van de PDF, of upload nog een PDF'


def generate_hash(file):
    temp_filepath = os.path.join('/tmp', file.filename)
    file.save(temp_filepath)

    with open(temp_filepath, 'rb') as file:
        content = file.read()
        digest = hashes.Hash(hashes.SHA256())
        digest.update(content)
        digestBytes = digest.finalize()
        sha256hash = digestBytes.hex()

    os.remove(temp_filepath)

    return sha256hash


def emit_progress_update(socketio, status, percentage):
    socketio.emit('update_progress', {'status': status, 'percentage': percentage})


def calculate_scores_for_magazine(magazine_id, session):
    magazine = get_magazine_by_id(magazine_id, session)
    pages = get_pages_by_magazine_id(magazine_id, session)
    antonyms = get_antonyms_from_database(session)

    reductionistic_words = [word.word for word in session.query(WordList).filter(WordList.type == 'reductionistic').all()]
    complex_words = [word.word for word in session.query(WordList).filter(WordList.type == 'complex').all()]

    complexity_scores = []
    reductionistic_scores = []

    for page in pages:
        text_data = page.text
        splitted_data = text_data.split()
        complex_count = 0
        reductionistic_count = 0

        # Loop through all split text
        for i in range(1, len(splitted_data)):
            current_word = splitted_data[i]
            previous_word = splitted_data[i - 1] if i > 0 else None

            # check word if it exists on reductionistic words
            if current_word in reductionistic_words:
                result = session.query(WordList.gewicht).filter(WordList.word == current_word).first()

                if result is not None:
                    gewicht = result[0]  # Take the weight out of the result

                    # check previous word if it exists on antonyms
                    if previous_word in antonyms:
                        complex_count += gewicht
                    else:
                        reductionistic_count += gewicht

            # check word if it exists on complex words
            elif current_word in complex_words:
                result = session.query(WordList.gewicht).filter(WordList.word == current_word).first()

                if result is not None:
                    gewicht = result[0]  # Take the weight out of the result

                    # check previous word if it exists on antonyms
                    if previous_word in antonyms:
                        reductionistic_count += gewicht
                    else:
                        complex_count += gewicht

        # Calculate complexity score: difference between counts of complex and reductionistic words
        complexity_score = complex_count - reductionistic_count

        if complexity_score > 0:
            complexity_scores.append(complexity_score)
        elif complexity_score < 0:
            reductionistic_scores.append(complexity_score)

    # Calculate average scores (avoid division by zero)
    average_complexity_score = round(sum(complexity_scores) / max(len(complexity_scores), 1), 1)
    average_reductionistic_score = round(sum(reductionistic_scores) / max(len(reductionistic_scores), 1), 1)

    magazine = Magazine(publication_date=magazine.publication_date, upload_date=magazine.upload_date,
                        title=magazine.title, complexity_score=average_complexity_score,
                        reductionistic_score=average_reductionistic_score)

    # Update magazine object with calculated scores
    magazine.complexity_score = average_complexity_score
    magazine.reductionistic_score = average_reductionistic_score

    # Post the updated magazine object to the database
    post_magazine(magazine, session)


def clean_dutch_text(text):
    # Tokenize the text into words
    words = word_tokenize(text, language='dutch')

    # get common dutch stopwords
    stop_words = list(stopwords.words('dutch'))

    # create list of most common names
    name_data = NameDataset()
    top_dutch_names_first_names = name_data.get_top_names(n=1000, country_alpha2='NL')
    top_dutch_names_last_names = name_data.get_top_names(n=1000, country_alpha2='NL', use_first_names=False)

    extra_words = ["patiënten", "patiënte", "fysiotherapie", "alle", "wel", "patiënt", "praktijk", "binnen", "tussen",
                   "gezondheid", "fysiotherapeut", "werden", "fysiopraxis", 'fysiotherapeuten', 'kngf', 'gaan', 'jaar',
                   'geven', 'komen', 'zo', 'model', 'lijkt', 'nederland', 'echter', 'dagen', 'toe', 'zowel', 'zoals',
                   "waar", "alleen", "sinds", "sprake", "werk", "onze", "den", "komt", "per", "bellen"]

    dutch_months = ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september',
                    'oktober', 'november', 'december']

    stop_words.extend(top_dutch_names_first_names)
    stop_words.extend(top_dutch_names_last_names)
    stop_words.extend(extra_words)
    stop_words.extend(dutch_months)

    filtered_words = [word for word in words if word.lower() not in stop_words]

    # Join the filtered words back into a cleaned text
    cleaned_text = ' '.join(filtered_words)

    return cleaned_text


def tokenize(text):
    # Tokenize the text into words
    words = word_tokenize(text, language='dutch')
    result = ' '.join(words)
    return result
