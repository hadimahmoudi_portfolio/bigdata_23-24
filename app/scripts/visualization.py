import base64
from io import BytesIO
from matplotlib import pyplot as plt
from wordcloud import WordCloud
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

import matplotlib
matplotlib.use('Agg')

def create_wordcloud(cleaned_text):
    try:
        wordcloud = WordCloud(width=800, height=400, background_color='white').generate_from_text(cleaned_text)
        return wordcloud
    except Exception as e:
        print(f"An error occurred while creating the word cloud: {e}")
        return None


def create_complexity_score_graph(pages, magazine_title):
    complexity_scores = []  # List to store complexity scores for all pages of the selected magazine
    for page in pages:
        complexity_scores.append({
            'page_number': page.page_number,
            'complexity_score': page.complexity_score
        })

    # Create Pandas DataFrame for Plotly graph
    df = pd.DataFrame(complexity_scores, columns=['page_number', 'complexity_score'])
    fig = px.line(df, x='page_number', y='complexity_score',
                  title=f'Complexiteit scores voor magazine {magazine_title}',
                  labels={'page_number': 'Pagina nummer', 'complexity_score': 'Complexiteit score'},
                  color_discrete_sequence=['red'])

    # Return the graph as HTML to be displayed in the route
    return fig.to_html(full_html=False)


def create_complexity_graph_through_years(magazines):
    data = []

    for magazine in magazines:
        data.append({
            'publication_date': magazine.publication_date,
            'title': magazine.title,
            'Score type': 'Complexiteit score',
            'score': magazine.complexity_score
        })
        data.append({
            'publication_date': magazine.publication_date,
            'title': magazine.title,
            'Score type': 'Reductionistische score',
            'score': magazine.reductionistic_score
        })

    # Create Pandas DataFrame for Plotly graph
    df = pd.DataFrame(data, columns=['publication_date', 'title', 'Score type', 'score'])

    # Sort DataFrame by publication_date
    df.sort_values('publication_date', inplace=True)

    fig = px.line(df, x='publication_date', y='score', color='Score type',
                  title='Complexiteit en reductionistische scores door de jaren heen',
                  labels={'publication_date': 'Publicatiedatum', 'score': 'Score'},
                  hover_data=['title'],
                  line_dash_sequence=['solid', 'dot'],
                  line_shape='linear',
                  color_discrete_map={'Complexiteit score': 'red', 'Reductionistische score': 'blue'})

    # Add a black line at y=0
    fig.add_shape(
        go.layout.Shape(
            type="line",
            x0=df['publication_date'].min(),
            x1=df['publication_date'].max(),
            y0=0,
            y1=0,
            line=dict(color="black", dash="100%"),
            opacity=0.7,
        )
    )

    # Return the graph as HTML to be displayed in the route
    return fig.to_html(full_html=False)


def plot_graph(wordcloud):
    # Plot the WordCloud and save it to a BytesIO buffer
    buffer = BytesIO()
    plt.figure(figsize=(8, 4))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.yticks(range(int(min(plt.yticks()[0])), int(max(plt.yticks()[0])) + 1), rotation=0)
    plt.savefig(buffer, format='png')
    plt.close()
    img_str = base64.b64encode(buffer.getvalue()).decode('utf-8')
    return img_str
