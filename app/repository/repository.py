from app.scripts.models.models import Magazine, Page, WordList, Antonyms
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.scripts.models.models import Base
from flask import jsonify

engine = create_engine('sqlite:///physio1.db')
Session = sessionmaker(bind=engine)
session = Session()


def init_db():
    Base.metadata.create_all(engine)


def post_magazine(magazine, session):
    try:
        existing_magazine = session.query(Magazine).filter_by(title=magazine.title).first()

        if existing_magazine:
            existing_magazine.publication_date = magazine.publication_date
            existing_magazine.upload_date = magazine.upload_date
            existing_magazine.title = magazine.title
            existing_magazine.complexity_score = magazine.complexity_score
            existing_magazine.reductionistic_score = magazine.reductionistic_score
            session.commit()
            return existing_magazine
        else:
            # Add new page
            session.add(magazine)
            session.commit()
            return magazine
    except Exception as e:
        print(f"Error occurred: {e}")
        session.rollback()  # Roll back changes in case of an error
        return False


def post_page(page, session):
    try:
        existing_page = get_page_by_magazine_id_and_page_number(page.magazine_id, page.page_number, session)

        if existing_page:
            existing_page.text = page.text
            existing_page.complexity_score = page.complexity_score
            session.commit()
            return existing_page
        else:
            # Add new page
            session.add(page)
            session.commit()
            return page
    except Exception as e:
        print(f"Error occurred: {e}")
        session.rollback()  # Roll back changes in case of an error
        return False


def get_magazines(session):
    all_magazines = session.query(Magazine).all()
    return all_magazines


def get_magazine_by_id(magazine_id, session):
    try:
        magazine = session.query(Magazine).filter(Magazine.id == magazine_id).first()
        return magazine
    except Exception as e:
        print(f"Error occurred: {e}")
        return None


def get_pages_by_magazine_id(magazine_id, session):
    try:
        pages = session.query(Page).filter(Page.magazine_id == magazine_id).all()
        return pages
    except Exception as e:
        print(f"Error occurred: {e}")
        return None


def get_page_by_magazine_id_and_page_number(magazine_id, page_number, session):
    try:
        page = session.query(Page).filter(Page.magazine_id == magazine_id, Page.page_number == page_number).first()
        return page
    except Exception as e:
        print(f"Error occurred: {e}")
        return None


def update_page(magazine_id, page_number, text, complexity_score, session):
    page_object = Page(text=text, page_number=page_number, complexity_score=complexity_score, magazine_id=magazine_id)
    post_page(page_object, session)


def delete_page(magazine_id, page_number, session):
    try:
        page = session.query(Page).filter(Page.magazine_id == magazine_id, Page.page_number == page_number).first()
        if page:
            session.delete(page)
            session.commit()
            return True
        else:
            return False
    except Exception as e:
        print(f"Error occurred: {e}")
        session.rollback()
        return False


def delete_magazine_by_magazine_id(magazine_id, session):
    try:
        magazine = session.query(Magazine).filter(Magazine.id == magazine_id).first()
        if magazine:
            # Delete associated pages aswell
            session.query(Page).filter(Page.magazine_id == magazine_id).delete()
            session.delete(magazine)

            session.commit()
            return True
        else:
            return False
    except Exception as e:
        print(f"Error occurred: {e}")
        session.rollback()
        return False


# Function to add antonyms in the database
def add_antonyms_to_database(antonyms, session):
    try:
        existing_antonyms = get_antonyms_from_database(session)
        added_antonyms = []

        if not existing_antonyms:
            new_antonym = Antonyms(antonym=antonyms)
            session.add(new_antonym)
            added_antonyms.append(antonyms)
        else:
            # Check if the provided antonym already exists in the database
            antonym_exists = any(antonyms == item.antonym for item in existing_antonyms)
            if not antonym_exists:
                new_antonym = Antonyms(antonym=antonyms)
                session.add(new_antonym)
                added_antonyms.append(antonyms)

        session.commit()

        if added_antonyms:
            feedback = f"Antonyms added successfully: {', '.join(added_antonyms)}"
            return jsonify({'success': True, 'feedback': feedback})
        else:
            feedback = "All provided antonyms already exist in the database."
            return jsonify({'success': False, 'feedback': feedback})

    except Exception as e:
        print(f"Error occurred: {e}")
        session.rollback()
        feedback = "Failed to add antonyms to the database."
        return jsonify({'success': False, 'feedback': feedback})


# Functie om antoniemen uit de database op te halen
def get_antonyms_from_database(session):
    antonyms = session.query(Antonyms).all()
    return antonyms


# Functie om woorden op te halen op basis van antoniemen
def get_words_by_type(word_type, session):
    words = session.query(WordList.word, WordList.gewicht).filter(WordList.type == word_type).all()
    return [(word.word, word.gewicht) for word in words]


def add_word_to_reductionistic_words(word, gewicht, word_type, session):
    existing_word = session.query(WordList).filter(WordList.word == word, WordList.gewicht == gewicht, WordList.type == 'reductionistic').first()
    if existing_word:
        if word_type != 'reductionistic':
            session.delete(existing_word)
            session.commit()
        else:
            existing_word.type = word_type  # Update existing word with new word type
    else:
        new_word = WordList(word=word, gewicht=gewicht, type=word_type)
        session.add(new_word)

    session.commit()


def add_word_to_complex_words(word, gewicht, word_type, session):
    existing_word = session.query(WordList).filter(WordList.word == word, gewicht == gewicht, WordList.type == 'complex').first()
    if existing_word:
        if word_type != 'complex':
            session.delete(existing_word)
            session.commit()
        else:
            existing_word.type = word_type  # Update existing word with new word type
    else:
        new_word = WordList(word=word, gewicht=gewicht, type=word_type)
        session.add(new_word)

    session.commit()


def delete_word(word, session):
    session.query(WordList).filter(WordList.word == word).delete()
    session.commit()


def delete_antonym(antonym, session):
    session.query(Antonyms).filter(Antonyms.antonym == antonym).delete()
    session.commit()