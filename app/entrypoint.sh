#!/bin/bash
exec gunicorn -b :5000 --timeout 120 --reload app:app #run Flask application "app" as "app" with gunicorn web server, bind available network interfaces on port 5000 and set --reload to auto reload on filechanges
