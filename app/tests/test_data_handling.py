import pytest
from sqlalchemy.orm import Session

from ..repository.repository import get_magazines, init_db, engine
from ..scripts.models.models import Base, Magazine


# Fixture to set up and tear down the database for testing
@pytest.fixture
def setup_database():
    init_db()  # Initialize the database
    yield Session(bind=engine)  # Provide the session to the tests
    Base.metadata.drop_all(engine)  # Clean up the database after testing


def test_get_magazines(setup_database):
    # Create some test data
    magazine1 = Magazine(title="Magazine 1")
    magazine2 = Magazine(title="Magazine 2")

    # Add the test data to the database
    setup_database.add(magazine1)
    setup_database.add(magazine2)
    setup_database.commit()

    # Call the function with the test session
    result = get_magazines(setup_database)

    # Assert that the function returned the expected result
    assert result == [magazine1, magazine2]
