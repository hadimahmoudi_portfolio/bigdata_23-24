import os

# Flask related imports
from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO

# Database related imports
from app.repository.repository import (get_magazines, get_magazine_by_id, get_page_by_magazine_id_and_page_number,
    get_pages_by_magazine_id, delete_magazine_by_magazine_id, session, init_db, update_page, delete_page,
                                       get_words_by_type, add_word_to_reductionistic_words, add_word_to_complex_words, delete_word,
                                       get_antonyms_from_database, add_antonyms_to_database, delete_antonym)

# Data preparation related imports
from app.scripts.data_handling import extract_and_upload_pdf, clean_dutch_text, calculate_scores_for_magazine, generate_hash

# Machine learning related imports
from app.scripts.machine_learning import calculate_complexity_score_from_text

# Visualization related imports
from app.scripts.visualization import (create_wordcloud, plot_graph, create_complexity_score_graph,
                                       create_complexity_graph_through_years)

# Flask app configuration settings
app = Flask(__name__)
app.config['DATABASE'] = './physio1.db'
app.config['TEMPLATES_AUTO_RELOAD'] = True
socketio = SocketIO(app)

# Only initialize a new database if there is no database present
if not os.path.isfile(app.config['DATABASE']):
    init_db()


### STARTING APP ROUTES ###

@app.route("/")
def index():
    return render_template('index.html')


@app.route("/pdf", methods=["POST", "GET"])
def show_upload_form_or_upload_pdf():
    if request.method == "POST":
        if 'pdfFile' in request.files:
            pdf_file = request.files['pdfFile']

            if pdf_file.filename != '':
                date_str = request.form['publication_date']
                feedback = extract_and_upload_pdf(pdf_file, date_str, socketio, session)
                all_magazines = get_magazines(session)[::-1]

        return render_template('pdf.html', feedback=feedback, magazines=all_magazines)
    else:
        all_magazines = get_magazines(session)[::-1]
        return render_template('pdf.html', magazines=all_magazines)


@app.route("/pdf/delete", methods=['POST'])
def delete_pdf():
    magazine_id = request.form['delete_magazine_id']

    # Check if the magazine with the given ID exists
    magazine = get_magazine_by_id(magazine_id, session)
    if magazine:
        feedback = f'PDF {magazine.title} succesvol verwijderd!'
        delete_magazine_by_magazine_id(magazine_id, session)
    else:
        feedback = 'PDF kan niet verwijderd worden, het is niet gevonden!'

    all_magazines = get_magazines(session)[::-1]
    return render_template('pdf.html', feedback=feedback, magazines=all_magazines)


@app.route("/bag-of-words", methods=['GET','POST'])
def bag_of_words():
    feedback = ""
    if request.method == 'POST':
        action = request.form.get('action')
        word_to_process = request.form.get('word_to_process')
        word_type = request.form.get('word_type')
        gewicht = request.form.get('gewicht')
        antonym_to_process = request.form.get('antonym_to_process')
        antonym_to_delete = request.form.get('antonym_to_delete')

        if action == 'add' and word_to_process:
            if word_type in ['reductionistic', 'complex']:
                if word_type == 'reductionistic':
                    add_word_to_reductionistic_words(word_to_process, gewicht, word_type, session)
                    feedback = f"Word '{word_to_process}' added successfully to reductionistic words!"
                    print(feedback)
                else:
                    add_word_to_complex_words(word_to_process, gewicht, word_type, session)
                    feedback = f"Word '{word_to_process}' added successfully to complex words!"
                    print(feedback)

                return jsonify({'success': True, 'feedback': feedback})

        elif action == 'delete' and word_to_process:
            delete_word(word_to_process, session)
            feedback = f"Word '{word_to_process}' deleted successfully from the list."
            print(feedback)
            return jsonify({'success': True, 'feedback': feedback})

        elif action == 'addAntonym' and antonym_to_process:
            add_antonyms_to_database(antonym_to_process, session)
            feedback = f"Word '{antonym_to_process}' added successfully to Antonym list."
            print(feedback)
            return jsonify({'success': True, 'feedback': feedback})

        elif action == 'delete_antonym' and antonym_to_delete:
            delete_antonym(antonym_to_delete, session)
            feedback = f"Word '{antonym_to_delete}' removed successfully from Antonym list."
            print(feedback)
            return jsonify({'success': True, 'feedback': feedback})

    reductionistic_words = get_words_by_type('reductionistic', session)
    complex_words = get_words_by_type('complex', session)

    # get antonyms database and send to front-end
    antonyms_list = get_antonyms_from_database(session)

    return render_template('bag_of_words.html', reductionistic_words=reductionistic_words,
                           complex_words=complex_words, feedback=feedback,
                           antonyms_list=antonyms_list)


@app.route("/magazine", methods=['POST', 'GET'])
def show_magazine_results():
    if request.method == "POST":
        all_magazines = get_magazines(session)
        if request.form.get('magazine_id') is None:
            return render_template('magazine.html', magazines=all_magazines)

        selected_magazine_id = int(request.form.get('magazine_id'))
        selected_magazine = get_magazine_by_id(selected_magazine_id, session)

        pages = get_pages_by_magazine_id(selected_magazine_id, session)
        all_text = ''.join(page.text for page in pages)
        cleaned_text = clean_dutch_text(all_text)
        wordcloud = create_wordcloud(cleaned_text)
        img_str = plot_graph(wordcloud) if wordcloud is not None else None
        graph_data = create_complexity_score_graph(pages, selected_magazine.title)

        magazine_results = {
            'complexity_graph': graph_data,
            'wordcloud_img': img_str
        }
        return render_template('magazine.html', magazines=all_magazines, selected_magazine=selected_magazine,
                               magazine_results=magazine_results)
    else:
        all_magazines = get_magazines(session)
        return render_template('magazine.html', magazines=all_magazines)


@app.route("/page", methods=['POST', 'GET'])
def show_pages():
    if request.method == "POST":
        all_magazines = get_magazines(session)
        if request.form.get('magazine_id') is None:
            return render_template('page.html', magazines=all_magazines)

        selected_magazine_id = int(request.form.get('magazine_id'))
        selected_magazine = get_magazine_by_id(selected_magazine_id, session)
        return render_template('page.html', magazines=all_magazines, selected_magazine=selected_magazine)
    else:
        all_magazines = get_magazines(session)
        return render_template('page.html', magazines=all_magazines)


@app.route("/page/results", methods=['POST'])
def show_page_results():
    all_magazines = get_magazines(session)

    # Get the selected magazine_id and page_number from the form
    selected_magazine_id = request.form.get('magazine_id')
    selected_page_number = int(request.form.get('page_number'))

    selected_magazine = get_magazine_by_id(selected_magazine_id, session)
    selected_page = get_page_by_magazine_id_and_page_number(selected_magazine_id, selected_page_number, session)

    cleaned_text = clean_dutch_text(selected_page.text)
    wordcloud = create_wordcloud(cleaned_text)

    if wordcloud is not None:
        img_str = plot_graph(wordcloud)
    else:
        img_str = None

    page_results = {
        'text': selected_page.text,
        'page_number': selected_page_number,
        'wordcloud_img': img_str,
        'complexity_score': selected_page.complexity_score
    }

    return render_template('page.html', magazines=all_magazines, selected_magazine=selected_magazine,
                           page_results=page_results)


@app.route("/page/results/update", methods=['POST'])
def page_update():
    text = request.form.get('taResults')
    magazine_id = int(request.form.get('magazine_id'))
    new_complexity_score = calculate_complexity_score_from_text(text, session)
    update_page(magazine_id, int(request.form.get('page_number')),
                text, new_complexity_score, session)
    calculate_scores_for_magazine(magazine_id, session)
    return show_page_results()


@app.route("/page/results/delete", methods=['POST'])
def page_delete():
    magazine_id = int(request.form.get('magazine_id'))
    delete_page(magazine_id, int(request.form.get('page_number')), session)
    calculate_scores_for_magazine(magazine_id, session)
    return show_pages()


@app.route("/complexity", methods=['GET'])
def complexity_scores_over_years():
    all_magazines = get_magazines(session)

    # Check if there are fewer than 5 magazines
    if len(all_magazines) < 5:
        feedback = ('Voor een correcte weergave is uploaden van meer PDF\'s vereist: minimaal 5 PDF\'s en voor optimaal'
                    'gebruik vaktijdschriften van verschillende publicatiedatums.')
        return render_template('complexity.html', feedback=feedback)
    else:
        graph_data = create_complexity_graph_through_years(all_magazines)
        return render_template('complexity.html', graph=graph_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
