# Project Big Data - semester 1 <2023>
Dear user, this tool is intended to extract insights from uploaded PDFs of PhysioPraxis. You can upload the desired PDF(s) under the heading 'Upload PDF'. You can then view results under the heading 'Vaktijdschriften'.

# Adding and using dependencies

If you use a external package, please add the package and the version number to the requirements.txt file in the current format: 
```python
{package name}=={package version}
```

When you are using a virtual environment, you can install the packages by running the following code (when in same directory as requirements file): 
```python
pip install -r requirements.txt
```


# Running Development image and container

## 1. Building and running Docker image and container with Docker Compose

Start the application the following way:

```PowerShell / Command Prompt (Terminal)
docker compose up -d
```

This command uses the compose.yaml file in the main directory to build the image according to the Dockerfile and associated commands and runs a container based on this image with set configurations.